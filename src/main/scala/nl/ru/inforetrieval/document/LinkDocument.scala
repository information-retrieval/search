package nl.ru.inforetrieval.document

class LinkDocument(val title: String, val citations: Iterable[String])
