package nl.ru.inforetrieval.document

import java.nio.charset.StandardCharsets
import java.nio.file.{Files, Paths}

object DocumentWriter {
    val PROP_SEP = ";"
    val TOKEN_SEP = ":"
    val LIST_SEP = ","
    val TITLE_SEP = "="
    val DOC_SEP = "\n"

    def stringRep(doc: Document): String = {
        doc.docId ++ PROP_SEP ++
            doc.hitLists.map(entry => s"${entry._1}$TOKEN_SEP" ++ entry._2.map(hit => hit.toHex).mkString(LIST_SEP)).mkString(PROP_SEP)
    }

    def write(path: String, docs: Iterable[Document]): Unit = {
        val str = docs.map(stringRep).mkString(DOC_SEP)
        Files.write(Paths.get(path), str.getBytes(StandardCharsets.UTF_8))
    }
}
