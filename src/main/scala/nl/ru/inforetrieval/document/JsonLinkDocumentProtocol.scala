package nl.ru.inforetrieval.document

import spray.json.{DefaultJsonProtocol, JsArray, JsNull, JsObject, JsString, JsValue, RootJsonFormat, deserializationError}

import scala.collection.mutable.ArrayBuffer

object JsonLinkDocumentProtocol extends DefaultJsonProtocol {
    def metadata(meta: JsObject): (String, String) = {
        val title: String = meta.getFields("title") match {
            case Seq(JsString(t)) => t
            case _ => ""
        }

        val authors: String = meta.getFields("authors") match {
            case Seq(JsArray(a)) => a.map({ author =>
                val authObj = author.asJsObject
                val last = authObj.getFields("last") match {
                    case Seq(JsString(str)) => str
                    case _ => ""
                }
                val first = authObj.getFields("first") match {
                    case Seq(JsString(str)) => str
                    case _ => ""
                }
                s"$last $first"
            }).mkString(" ")
            case _ => ""
        }

        (title, authors)
    }

    def references(obj: Iterable[JsValue]): Iterable[String] = {
        val titles = new ArrayBuffer[String]
        obj.map(jsVal => jsVal.asJsObject.getFields("title") match {
            case Seq(JsString(title)) => titles.addOne(title)
            case _ =>
        })
        titles
    }

    implicit object JsonLinkDocumentFormat extends RootJsonFormat[LinkDocument] {
        def read(value: JsValue): LinkDocument = {
            value.asJsObject.getFields("metadata", "bib_entries") match {
                case Seq(meta: JsObject, JsObject(refs)) =>
                    val md = metadata(meta)
                    val cites = references(refs.values)

                    new LinkDocument(md._1, cites)
                case obj => throw deserializationError(s"could not deserialize document, found $obj")
            }
        }

        override def write(doc: LinkDocument): JsValue = JsNull
    }

}
