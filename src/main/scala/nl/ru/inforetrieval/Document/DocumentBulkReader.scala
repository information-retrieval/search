package nl.ru.inforetrieval.document

import scala.collection.immutable.Set
import scala.collection.parallel.CollectionConverters._
import scala.collection.parallel.immutable.ParSet

class DocumentBulkReader[T](documents: Set[String], readSingleFile: String => T) {
    var documentSet: ParSet[String] = documents.par

    def setDocuments(documents: Set[String]): DocumentBulkReader[T] = {
        documentSet = documents.par
        this
    }

    def processDocuments(): Set[T] = {
        documentSet.map(readSingleFile).seq
    }
}
