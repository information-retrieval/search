package nl.ru.inforetrieval.document

import java.io.File
import scala.collection.mutable

class DocumentRunner {

    def getListOfFiles(dir: String): Option[mutable.HashSet[String]] = {
        try {
            val file = new File(dir)
            val documentSet = new mutable.HashSet[String]()
            documentSet.addAll(file.listFiles.filter(_.isFile)
                .filter(_.getName.endsWith(".json"))
                .map(_.getPath))
            Some(documentSet)
        }
        catch {
            case e: Exception => {
                println("Failed to open file"); None;
            }
        }
    }
}
