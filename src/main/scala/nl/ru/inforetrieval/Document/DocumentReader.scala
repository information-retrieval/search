package nl.ru.inforetrieval.document

import spray.json._

import scala.io.{BufferedSource, Source}
import JsonDocumentProtocol._

object DocumentReader {
    def readFromPath(path: String): Document = {
        val file: BufferedSource = Source.fromFile(path)
        try {
            readFromString(file.mkString(""))
        } finally {
            file.close()
        }
    }

    def readFromString(str: String): Document = {
        val json: JsValue = str.parseJson
        json.convertTo[Document]
    }
}
