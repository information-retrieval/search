package nl.ru.inforetrieval.document

import nl.ru.inforetrieval.document.Document.generateHitLists
import nl.ru.inforetrieval.hits.HitType.{Abstract, Author, Fancy, HitType, Plain}
import nl.ru.inforetrieval.hits.{CompactHit, Hit, HitType}

import scala.collection.mutable

class Document(val docId: String, val title: String, authors: String, abstr: String, body: String) {
    val hitLists: Map[String, Iterable[Hit]] = generateHitLists(title, authors, abstr, body)

    def tokens: Iterable[String] = hitLists.keys

    val lengths: (Int, Int) = { // (abstract length, body length)
        val grouped = hitLists.values.flatten.groupBy(_.hitType)
            (grouped.get(Abstract).map(x => x.size).getOrElse(0), grouped.get(Plain).map(x => x.size).getOrElse(1))

    }
}

object Document {
    def generateHitLists(title: String, authors: String, abstr: String, body: String): Map[String, Iterable[Hit]] = {
        val tokens: Array[Array[String]] = Array(title, authors, abstr, body).map(str => tokenize(str))
        val hLists = new mutable.HashMap[String, mutable.ArrayBuffer[Hit]]

        var index: Int = 0
        for (cat <- tokens.zip(Array(Fancy, Author, Abstract, Plain))) {
            index = generateHits(hLists, cat._1, cat._2, index)
        }

        hLists.toMap
    }

    private def generateHits(hitLists: mutable.HashMap[String, mutable.ArrayBuffer[Hit]], tokens: Array[String], hitType: HitType, offset: Int): Int = {
        tokens match {
            case Array("") => offset
            case tokens =>
                var off: Int = offset
                tokens.foreach(token => {
                    val lower = token.toLowerCase()
                    hitLists.get(lower) match {
                        case Some(list) => list.addOne(new CompactHit(hitType, off))
                        case None => hitLists.addOne(lower, mutable.ArrayBuffer(new CompactHit(hitType, off)))
                    }

                    off += 1
                })
                off
        }
    }

    private def tokenize(text: String): Array[String] = {
        text.toLowerCase.split("[^a-zA-Z0-9]").filter(str => str.nonEmpty)
    }
}
