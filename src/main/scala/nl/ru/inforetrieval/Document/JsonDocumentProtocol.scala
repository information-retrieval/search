package nl.ru.inforetrieval.document

import spray.json._

object JsonDocumentProtocol extends DefaultJsonProtocol {
    def metadata(meta: JsObject): (String, String) = {
        val title: String = meta.getFields("title") match {
            case Seq(JsString(t)) => t
            case _ => ""
        }

        val authors: String = meta.getFields("authors") match {
            case Seq(JsArray(a)) => a.map({ author =>
                val authObj = author.asJsObject
                val last = authObj.getFields("last") match {
                    case Seq(JsString(str)) => str
                    case _ => ""
                }
                val first = authObj.getFields("first") match {
                    case Seq(JsString(str)) => str
                    case _ => ""
                }
                s"$last $first"
            }).mkString(" ")
            case _ => ""
        }

        (title, authors)
    }

    def text(obj: JsArray): String = {
        obj match {
            case JsArray(as) => as.map(jsVal => jsVal.asJsObject.getFields("text") match {
                case Seq(JsString(text)) => text
                case _ => ""
            }).mkString(" ")
            case _ => ""
        }
    }


    implicit object JsonDocumentFormat extends RootJsonFormat[Document] {
        def read(value: JsValue): Document = {
            value.asJsObject.getFields("paper_id", "metadata", "abstract", "body_text", "bib_entries") match {
                case Seq(JsString(id), meta: JsObject, bodyArr: JsArray, JsObject(refs)) =>
                    val md = metadata(meta)
                    val body = text(bodyArr)

                    new Document(id, md._1, md._2, "", body)
                case Seq(JsString(id), meta: JsObject, abstractObj: JsArray, bodyArr: JsArray, JsObject(refs)) =>
                    val md = metadata(meta)
                    val abstr = text(abstractObj)
                    val body = text(bodyArr)

                    new Document(id, md._1, md._2, abstr, body)
                case obj => throw deserializationError(s"could not deserialize document, found $obj")
            }
        }

        override def write(doc: Document): JsValue = JsNull
    }

}