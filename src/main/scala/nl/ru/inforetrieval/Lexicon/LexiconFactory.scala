package nl.ru.inforetrieval.lexicon

import nl.ru.inforetrieval.document.Document

class LexiconFactory {

    def getNewEmptyLexicon(): Lexicon = new HashedLexicon()

    def getLexiconFromDocuments(documents: Iterable[Document]): Lexicon = {
        val lexicon = new HashedLexicon()
        documents.foreach(document => {
            document.tokens.foreach(token => lexicon.addWord(token))
        })
        lexicon
    }
}
