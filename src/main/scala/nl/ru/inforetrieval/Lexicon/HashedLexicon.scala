package nl.ru.inforetrieval.lexicon

import scala.collection.mutable
import java.io._
import scala.io.Source

class HashedLexicon extends Lexicon {

    private val hashMap = mutable.HashMap[String, Int]()

    override def addWord(word: String): Int = {
        if(!hashMap.contains(word))
            hashMap.addOne((word, word.hashCode))
        getIdOfWord(word)
    }

    override def addWord(word: String, wordId: Int): Int = {
        hashMap.addOne((word, wordId)).apply(word)
    }

    override def getIdOfWord(word: String): Int = {
        hashMap.apply(word)
    }

    override def writeToFile(fileName: String): Unit = {
        val pw = new PrintWriter(new File(fileName ))

        hashMap.foreachEntry {
            case (word: String, wordId: Int) =>
                pw.println(word + "," + wordId.toString)
        }
        pw.close()
    }

    override def buildFromFile(fileName: String): Unit = {
        hashMap.clear()

        val fSource = Source.fromFile(fileName)
        for(line <- fSource.getLines){
            val wordIdTuple = line.split(',')
            hashMap.addOne((wordIdTuple(0), Integer.parseInt(wordIdTuple(1))))
        }

        fSource.close()
    }

    override def getRawMap = hashMap.toMap


    //private def getMd5(inputStr: String): Int = {
    //    val md: MessageDigest = MessageDigest.getInstance("MD5")
    //    val digest = md.digest(inputStr.getBytes())
    //    ByteBuffer.wrap(digest).getInt
    //}
}
