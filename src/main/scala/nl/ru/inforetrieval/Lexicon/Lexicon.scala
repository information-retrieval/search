package nl.ru.inforetrieval.lexicon

trait Lexicon {
    def addWord(word: String): Int
    def addWord(word: String, wordId: Int): Int
    def getIdOfWord(word: String): Int
    def writeToFile(fileName: String): Unit
    def buildFromFile(fileName: String): Unit
    def getRawMap: Map[String, Int]
}
