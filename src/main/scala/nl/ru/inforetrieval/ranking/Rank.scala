package nl.ru.inforetrieval.ranking

import nl.ru.inforetrieval.barrel.BarrelEntry
import scala.collection.mutable

trait Rank {
    def rank(barrelEntries: mutable.Seq[BarrelEntry]): mutable.Seq[(BarrelEntry, Double)]
}
