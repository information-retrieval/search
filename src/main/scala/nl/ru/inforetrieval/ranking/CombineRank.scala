package nl.ru.inforetrieval.ranking

import scala.collection.mutable

class CombineRank[T] {

    def calculateCombinedRanking(ranking1: mutable.Seq[(T, Double)], ranking2: mutable.Seq[(T, Double)]): mutable.Seq[(T, Double)] = {
        if (ranking1.length != ranking2.length)
            throw new IllegalArgumentException

        normalize(ranking1).zip(normalize(ranking2)).map(rankings => {
            (rankings._1._1, 0.5 * rankings._1._2 + 0.5 * rankings._2._2)
        }).sortBy(x => x._2).reverse
    }

    def normalize(ranking: mutable.Seq[(T, Double)]): mutable.Seq[(T, Double)] ={
        val doubleValues = ranking.map(x => x._2)
        val min = doubleValues.min
        val range = doubleValues.max - min
        ranking.map(rank => (rank._1, (rank._2 - min) / range))
    }
}
