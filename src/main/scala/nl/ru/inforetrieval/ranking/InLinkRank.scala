package nl.ru.inforetrieval.ranking
import nl.ru.inforetrieval.barrel.BarrelEntry

import scala.collection.immutable.HashMap
import scala.collection.mutable

class InLinkRank(val inLinks: HashMap[String,Int]) extends Rank {
    override def rank(barrelEntries: mutable.Seq[BarrelEntry]): mutable.Seq[(BarrelEntry, Double)] = {
        barrelEntries.map(entry => {
            (entry, inLinks.getOrElse(entry.doc.title, 0).toDouble)
        }).sortBy(x => x._1.doc.docId)
    }
}
