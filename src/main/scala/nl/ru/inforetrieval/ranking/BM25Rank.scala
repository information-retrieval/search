package nl.ru.inforetrieval.ranking

import nl.ru.inforetrieval.barrel.BarrelEntry
import nl.ru.inforetrieval.hits.HitType
import scala.collection.mutable

class BM25Rank(val numberOfDocuments: Integer, val avgLengths: (Double, Double)) extends Rank{

    override def rank(barrelEntries: mutable.Seq[BarrelEntry]) : mutable.Seq[(BarrelEntry, Double)] = {
        barrelEntries.map(barrelEntry  => {
            (barrelEntry, calculateScore(barrelEntry, barrelEntries.length, avgLengths))
        }).sortBy(x => x._1.doc.docId)
    }

    private def calculateScore(entry: BarrelEntry, documentsContainingQueury: Int, avgLengths: (Double, Double)): Double = {
        val k = 1.2
        (((k+1) * weightedHitCount(entry, avgLengths)) / (k + weightedHitCount(entry, avgLengths))) * idf(documentsContainingQueury)
    }

    private def idf(documentsContainingQuery: Int): Double =
        math.log((numberOfDocuments - documentsContainingQuery + 0.5)/(documentsContainingQuery + 0.5) + 1)

    private def weightedHitCount(entry: BarrelEntry, avgLengths: (Double, Double)): Double= {
        val b = 0.75
        //Array(HitType.Abstract, HitType.Plain).map(hitType => {
        //}).sum
        //Array(HitType.Author, HitType.Fancy).map.sum

        val weightAbstract = 0.25 * entry.hitList.count(x => x.hitType == HitType.Abstract)/ (1 - b + entry.doc.lengths._1/(b*avgLengths._1))
        val weightPlain = 0.15 * entry.hitList.count(x => x.hitType == HitType.Plain)/ (1 - b + entry.doc.lengths._2/(b*avgLengths._2))

        val weightFancy = 0.25 * entry.hitList.count(x => x.hitType == HitType.Fancy)
        val weightAuthor = 0.35 * entry.hitList.count(x => x.hitType == HitType.Author)

        weightAbstract + weightPlain + weightFancy + weightAuthor
    }

    // Field 1: Abstract = 0.25
    // Field 2: Plain = 0.15
    // Field 3: Fancy = 0.25
    // Field 4: Authors = 0.35
}
