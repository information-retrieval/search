package nl.ru.inforetrieval

import nl.ru.inforetrieval.barrel.{BarrelEntry, BarrelFactory, BarrelRepository}
import nl.ru.inforetrieval.document.{DocumentBulkReader, DocumentReader, DocumentRunner, DocumentWriter}
import nl.ru.inforetrieval.lexicon.{Lexicon, LexiconFactory}
import nl.ru.inforetrieval.document.JsonLinkDocumentProtocol.JsonLinkDocumentFormat
import nl.ru.inforetrieval.document._
import nl.ru.inforetrieval.lexicon.LexiconFactory
import spray.json._
import java.io.{FileInputStream, FileOutputStream, ObjectInputStream, ObjectOutputStream}

import nl.ru.inforetrieval.ranking._

import scala.collection.immutable.HashMap
import scala.collection.mutable
import scala.collection.parallel.CollectionConverters.ArrayIsParallelizable
import scala.io.Source

object Main {
    def time(f: () => Unit): Unit = {
        val t1 = System.nanoTime()
        f()
        val t2 = System.nanoTime()
        println("")
        println(s"${(t2 - t1) / 1_000_000} ms")
    }

    def groupedDocs(paths: Iterable[String]): Iterator[Iterable[LinkDocument]] = {
        paths.grouped(1000).map(docPaths => {
            val docs = docPaths.map(path => {
                val file = Source.fromFile(path)
                try {
                    file.mkString("").parseJson.convertTo[LinkDocument]
                } finally {
                    file.close()
                }
            })
            docs
        })
    }

    def countInLinks(paths: Set[String]): Map[String, Int] = {
        val reader = new DocumentBulkReader[LinkDocument](paths, path => {
            val file = Source.fromFile(path)
            try {
                file.mkString("").parseJson.convertTo[LinkDocument]
            } finally {
                file.close()
            }
        })

        val counts = new mutable.HashMap[String, Int]
        reader.processDocuments().foreach(doc => if (!doc.title.isBlank) counts(doc.title) = 0)
        println(s"found ${counts.size} titles, counting titles...")
        reader.processDocuments().foreach(doc => {
            doc.citations.foreach(ref => {
                counts.get(ref) match {
                    case Some(_) => counts(ref) += 1
                    case None =>
                }
            })
        })

        counts.toMap
    }

    def prepareCounts(): Unit = {
        val runner = new DocumentRunner()
        val docPaths: mutable.HashSet[String] = runner.getListOfFiles("data") match {
            case Some(listOfFiles) => listOfFiles
            case None => new mutable.HashSet[String]()
        }
        println(s"found ${docPaths.size} files")
        val counts = countInLinks(docPaths.toSet)
        println(s"citations counted, total inlink count ${counts.values.sum}")
        val stream = new ObjectOutputStream(new FileOutputStream("cached/counts.dat"))
        stream.writeObject(counts)
        println("counts written to /cached/counts")
    }

    def main(args: Array[String]): Unit = {
        print(Runtime.getRuntime.maxMemory / 1024.0 / 1024 / 1024)
        print(" GB of heap space\r\n")
        val (o,a) = getInputParams()
        simpleQuery(o,a)
    }

    def simpleQuery(useReference: Boolean, amountOfPapers: Int): Unit = {
        println("Reading documents...")
        val runner = new DocumentRunner()
        val documentsSet: mutable.HashSet[String] = runner.getListOfFiles("data") match {
            case Some(listOfFiles) => listOfFiles.take(amountOfPapers)
            case None => new mutable.HashSet[String]()
        }

        val bulkReader = new DocumentBulkReader[Document](documentsSet.toSet, DocumentReader.readFromPath)
        val documents = bulkReader.processDocuments()

        var inLinkCounts = new HashMap[String, Int]
        if(useReference)
            inLinkCounts = readInLinkCounts()
        println("Successfully processed all documents")

        println("Filling lexicon")
        val lexicon = new LexiconFactory().getLexiconFromDocuments(documents)
        println("Filling barrels")

        val sums: (Int, Int) = documents.foldLeft((0, 0))((sums, doc) => (doc.lengths._1 + sums._1, doc.lengths._2 + sums._2))
        val avgLengths:(Double, Double) = (sums._1 / documents.size.toDouble, sums._2 / documents.size.toDouble)
        val documentPartitions = documents.grouped(15).toArray.par // 50 = #documents in each thread

        val repos = documentPartitions.map(partition => {
            val barrelRepository = new BarrelRepository(new BarrelFactory())
            barrelRepository.generateForwardBarrels()

            partition.foreach(document => {
                document.tokens
                    .foreach(tok => barrelRepository.processWord(document, lexicon.getIdOfWord(tok), document.hitLists(tok).toBuffer))
            })
            barrelRepository
        }).seq

        println("Combining the repos")
        val barrelRepository = repos.reduce((r1, r2) => r1.concat(r2))

        println("Generate inverted barrels")
        barrelRepository.generateInvertedBarrels()
        println("Ready to start searching!")

        while (true) {
            println("Please enter search query. Leave empty to stop.")
            val query = scala.io.StdIn.readLine()
            if (query == null || query == "") {
                println("Shutting down.......")
                return
            }
            val t1 = System.nanoTime()


            val wordId = lexicon.getIdOfWord(query)
            val searchResult = barrelRepository.searchWord(wordId)

            val bm25Ranker = new BM25Rank(documents.size, avgLengths)
            val sortedBM25FBarrels = bm25Ranker.rank(searchResult)
            val inlinkRanker = new InLinkRank(inLinkCounts)
            val sortedinLinkBarrels = inlinkRanker.rank(searchResult)

            val combineRanker = new CombineRank[BarrelEntry]
            val sortedCombinedBarrels = combineRanker.calculateCombinedRanking(sortedBM25FBarrels, sortedinLinkBarrels)

            val t2 = System.nanoTime()
            println("Found " + sortedCombinedBarrels.length.toString + s" match(es) in ${(t2 - t1) / 1_000_000} ms")
            println("In documents: ")
            sortedCombinedBarrels.foreach(x => print(x._1.doc.docId + ", "))
            println("")
        }
    }

    def examineLexiconContens(lexicon: Lexicon): Unit = {
        val lexiconMap = lexicon.getRawMap
        val groupedLexidon = lexiconMap.groupMapReduce(tuple => tuple._2)(_ => 1)((firstElement, secondElement) => firstElement + secondElement).filter(idAndCount => idAndCount._2 > 1)
        println("Lexicon size: " + lexiconMap.size)
        println("Number of collisions: " +groupedLexidon.size)
    }

    def getInputParams(): (Boolean, Int) = {
        println("Welcome to the search engine")
        println("If you want to use In-Link count as ranking, please enter 'y', else 'n'")
        val option = scala.io.StdIn.readLine().toLowerCase()
        if(option == "y")
            println("Ok. We are using In-Link count as ranking")
        else
            println("Ok. We are using BM25F as ranking")

        println("How many documents do you want to process? Enter integer between 1000 and 190.000")
        val amount = scala.io.StdIn.readInt()
        println(s"Ok. We are going to process $amount documents.")

        (option == "y", amount)
    }

    def readInLinkCounts(): HashMap[String,Int] ={
        val input = new ObjectInputStream(new FileInputStream("cached/counts.dat"))
        val obj = input.readObject()
        obj match{
            case x if obj.isInstanceOf[HashMap[String,Int]] => x.asInstanceOf[HashMap[String,Int]]
            case _ => throw new Exception("Could not deserialize inlink count object  ")
        }
    }
}
