package nl.ru.inforetrieval.hits

import spray.json.{DefaultJsonProtocol, JsNull, JsValue, RootJsonFormat}

object JsonHitProtocol extends DefaultJsonProtocol {

    implicit object JsonHitFormat extends RootJsonFormat[Hit] {
        def read(value: JsValue): Hit = new CompactHit(value.convertTo[Int])

        override def write(hit: Hit): JsValue = JsNull
    }

}