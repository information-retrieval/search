package nl.ru.inforetrieval.hits

import nl.ru.inforetrieval.hits.CompactHit.encoding
import nl.ru.inforetrieval.hits.HitType.{Abstract, Author, Fancy, HitType, Plain}

object CompactHit {
    def encoding(htype: HitType, pos: Int): Int = {
        var enc: Int = htype match {
            case Abstract => 0x0000_0003
            case Author => 0x0000_0002
            case Fancy => 0x0000_0001
            case _ => 0x0000_0000
        }
        enc = enc | (pos << 2)
        enc
    }

    def fromHex(str: String): Hit = new CompactHit(Integer.parseInt(str, 16))
}

class CompactHit(private val enc: Int) extends Hit {
    def this(htype: HitType, pos: Int) = this(encoding(htype, pos))

    override def hitType: HitType = enc & 0x0000_0003 match {
        case 0x0000_0003 => Abstract
        case 0x0000_0002 => Author
        case 0x0000_0001 => Fancy
        case 0x0000_0000 => Plain
    }

    override def position: Int = (enc & 0xffff_fffc) >> 2

    def toHex: String = enc.toHexString
}
