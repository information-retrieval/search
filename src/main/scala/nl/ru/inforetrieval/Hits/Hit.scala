package nl.ru.inforetrieval.hits

import nl.ru.inforetrieval.hits.HitType.HitType

abstract class Hit {
    def hitType: HitType

    def position: Int

    override def toString: String = s"Hit($hitType, $position)"

    override def equals(obj: Any): Boolean = {
        obj match {
            case other: Hit => hitType == other.hitType && position == other.position
            case _ => false
        }
    }

    override def hashCode(): Int = {
        var result = 17
        result = 31 * result + hitType.hashCode()
        result = 31 * result + position.hashCode()
        result
    }

    def toHex: String
}
