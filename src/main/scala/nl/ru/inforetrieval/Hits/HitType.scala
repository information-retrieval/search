package nl.ru.inforetrieval.hits

object HitType extends Enumeration {
    type HitType = Value
    val Plain, Fancy, Abstract, Author = Value
}