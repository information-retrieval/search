package nl.ru.inforetrieval.barrel

import nl.ru.inforetrieval.document.Document
import nl.ru.inforetrieval.hits.Hit

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

trait Barrel {

    def getStart: Int

    def getEnd: Int

    def addHits(doc: Document, wordId: Int, hits: mutable.Buffer[Hit]): Unit

    def removeBarrelEntry(entry: BarrelEntry): Unit

    def getBarrelEntry(docId: String, wordId: Int): Option[BarrelEntry]

    def getAllEntries: ArrayBuffer[BarrelEntry]

    def concat(barrel: Barrel): Barrel
}
