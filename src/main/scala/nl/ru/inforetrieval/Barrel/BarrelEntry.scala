package nl.ru.inforetrieval.barrel

import nl.ru.inforetrieval.document.Document
import nl.ru.inforetrieval.hits.Hit

import scala.collection.mutable

class BarrelEntry(val doc: Document, val wordId: Int, val hitList: mutable.Buffer[Hit]) extends Ordered[BarrelEntry] {

    def addWordHit(hit: Hit): Unit ={
        hitList.addOne(hit)
    }

    def compare(that: BarrelEntry): Int = {
        if (this.wordId == that.wordId)
            this.hitList.length.compare(that.hitList.length) * -1
        else if (this.wordId > that.wordId)
            1
        else
            -1
    }
}
