package nl.ru.inforetrieval.barrel

import nl.ru.inforetrieval.document.Document
import nl.ru.inforetrieval.hits.Hit

import scala.collection.mutable
import scala.collection.mutable.{ArrayBuffer, ListBuffer}

class BarrelRepository(barrelFactory: BarrelFactory, var forwardBarrels: Array[ForwardBarrel] = new Array[ForwardBarrel](0)) {

    private var invertedBarrels: Array[InvertedBarrel] = _

    def generateForwardBarrels(): Unit = {
        forwardBarrels = barrelFactory.generateForwardBarrels()
        // printf("Generated %s forwardBarrels\n", forwardBarrels.length)
    }
    def generateInvertedBarrels(): Unit ={
        invertedBarrels = barrelFactory.generateInvertedBarrels(forwardBarrels)
        // printf("Generated %s invertedBarrels\n", invertedBarrels.length)
    }

    def getForwardBarrels: Array[ForwardBarrel] = forwardBarrels

    def processWord(doc: Document, wordId: Int, hits: mutable.Buffer[Hit]): Unit = {
        val barrel = getBarrelForWordId(wordId)
        barrel.addHits(doc, wordId, hits)
        // hits.foreach(x => barrel.addEntry(docId, wordId, x))
    }

    def searchWord(wordId: Int): ArrayBuffer[BarrelEntry] = {
        val barrel = getInvertedBarrelForWordId(wordId)
        barrel.getAllEntries.filter(x => x.wordId == wordId)
    }

    def concat(barrelRepository: BarrelRepository): BarrelRepository = {
        if(forwardBarrels.length != barrelRepository.getForwardBarrels.length)
            throw new IllegalArgumentException("Concatenating two BarrelRepositories requires the same number of ForwardBarrels!")

        new BarrelRepository(
            barrelFactory,
            forwardBarrels.zip(barrelRepository.getForwardBarrels)
                .map(barrelsPair => barrelsPair._1.concat(barrelsPair._2))
        )
    }

    private def getBarrelForWordId(wordId: Int): Barrel = {
        // forwardBarrels.find(x => x.getStart <= wordId && x.getEnd >= wordId).get
        forwardBarrels(barrelFactory.getBarrelKeyForWordId(wordId))
    }

    private def getInvertedBarrelForWordId(wordId: Int): InvertedBarrel = {
        //invertedBarrels.filter(x => x.getStart <= wordId && x.getEnd >= wordId).head
        invertedBarrels.find(x => x.getStart <= wordId && x.getEnd >= wordId).get
    }

}
