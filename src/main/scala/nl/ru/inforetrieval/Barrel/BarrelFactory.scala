package nl.ru.inforetrieval.barrel

class BarrelFactory {
    val numberOfBarrels: Int = 64
    private val halfNumberOfBarrels: Int = numberOfBarrels / 2
    val barrelSize: Int = Int.MaxValue / (numberOfBarrels/2) + 1

    def generateForwardBarrels(): Array[ForwardBarrel] = {
        (0 until numberOfBarrels).toArray.map(x =>
            new ForwardBarrel(Int.MinValue + x * barrelSize, Int.MinValue + (x * barrelSize) + barrelSize - 1)
        )
    }

    def generateInvertedBarrels(forwardBarrels: Array[ForwardBarrel]): Array[InvertedBarrel] = {
        forwardBarrels.map(x => new InvertedBarrel(x))
    }

    def getBarrelKeyForWordId(id: Int): Int = ((id.toDouble / barrelSize) + halfNumberOfBarrels).toInt
}
