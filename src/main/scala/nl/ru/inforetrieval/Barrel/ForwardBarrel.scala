package nl.ru.inforetrieval.barrel

import nl.ru.inforetrieval.hits.Hit

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer
import nl.ru.inforetrieval.document.Document

class ForwardBarrel(val start: Int, val end: Int, val entries: ArrayBuffer[BarrelEntry] = new ArrayBuffer[BarrelEntry]()) extends Barrel {

    override def getStart: Int = start
    override def getEnd: Int = end

    override def addHits(doc: Document, wordId: Int, hits: mutable.Buffer[Hit]): Unit = {
        getBarrelEntry(doc.docId, wordId) match {
            case Some(foundEntry) => hits.foreach(hit => foundEntry.addWordHit(hit))
            case None => entries.addOne(new BarrelEntry(doc, wordId, hits))
        }
    }

    override def removeBarrelEntry(entry: BarrelEntry): Unit = entries -= entry

    override def getBarrelEntry(docId: String, wordId: Int): Option[BarrelEntry] =
        entries.find(x => x.doc.docId == docId && x.wordId == wordId)

    override def getAllEntries: ArrayBuffer[BarrelEntry] = entries

    override def concat(barrel: Barrel): ForwardBarrel = {
        if(start != barrel.getStart || end != barrel.getEnd)
            throw new IllegalArgumentException("Concatenating two barrels requires the same start and end values!")

        new ForwardBarrel(start, end, entries.concat(barrel.getAllEntries))
    }
}
