package nl.ru.inforetrieval.barrel

import nl.ru.inforetrieval.document.Document
import nl.ru.inforetrieval.hits.Hit

import scala.collection.mutable
import scala.collection.mutable.ArrayBuffer

class InvertedBarrel(barrel: Barrel) extends Barrel {

    private val entries: ArrayBuffer[BarrelEntry] = barrel.getAllEntries.sorted

    override def getStart: Int = barrel.getStart

    override def getEnd: Int = barrel.getEnd

    override def addHits(doc: Document, wordId: Int, hits: mutable.Buffer[Hit]): Unit = {
        throw new NotImplementedError()
    }

    override def removeBarrelEntry(entry: BarrelEntry): Unit = {
        throw new NotImplementedError()
    }

    override def getBarrelEntry(docId: String, wordId: Int): Option[BarrelEntry] =
        entries.find(x => x.doc.docId == docId && x.wordId == wordId)

    def getDocsForWordId(wordId: Int): List[BarrelEntry] =
        entries.filter(x => x.wordId == wordId).toList

    override def getAllEntries: ArrayBuffer[BarrelEntry] = entries

    override def concat(barrel: Barrel): InvertedBarrel = {
        throw new NotImplementedError()
    }

}
