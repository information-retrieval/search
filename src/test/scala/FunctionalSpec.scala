import nl.ru.inforetrieval.barrel.{BarrelFactory, BarrelRepository}
import nl.ru.inforetrieval.document.DocumentReader
import nl.ru.inforetrieval.lexicon.HashedLexicon
import org.scalatest.freespec.AnyFreeSpec

class FunctionalSpec extends AnyFreeSpec {

    "The main feature must work" in {
        mainFeature()
    }

    def mainFeature():Unit = {
        val doc = DocumentReader.readFromPath("data/0000028b5cc154f68b8a269f6578f21e31f62977.json")
        val doc2 = DocumentReader.readFromPath("data/0000028b5cc154f68b8a269f6578f21e31f62977.json")

        val hashedLexicon = new HashedLexicon()
        doc.tokens.foreach(tok => hashedLexicon.addWord(tok))

        val barrelRepository = new BarrelRepository(new BarrelFactory())
        barrelRepository.generateForwardBarrels()

        doc.tokens
            .foreach(tok => barrelRepository.processWord(doc, hashedLexicon.getIdOfWord(tok), doc.hitLists(tok).toBuffer))
        doc2.tokens
            .foreach(tok => barrelRepository.processWord(doc2, hashedLexicon.getIdOfWord(tok), doc.hitLists(tok).toBuffer))

        barrelRepository.generateInvertedBarrels()

        val query = "for"
        val wordId = hashedLexicon.getIdOfWord(query)
        val searchResult = barrelRepository.searchWord(wordId)
        assert(searchResult.size == 2)
        assert(searchResult.head.wordId == 101577)
        //assert(searchResult.head.score >= 49)

//        val hashLexicon = new HashedLexicon
//        hashLexicon.addWord("test")
//        val a = hashLexicon.getIdOfWord("test")
//        println(a)
//
//        hashLexicon.writeToFile("lexicon.txt")
//        hashLexicon.buildFromFile("lexicon.txt")
//        println(hashLexicon.getIdOfWord("test"))
    }
}
