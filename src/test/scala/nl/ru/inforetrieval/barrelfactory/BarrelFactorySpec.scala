package nl.ru.inforetrieval.barrelfactory

import nl.ru.inforetrieval.barrel.{Barrel, BarrelFactory, ForwardBarrel}
import org.scalatest.freespec.AnyFreeSpec

class BarrelFactorySpec extends AnyFreeSpec{

    "The barrelFactory will produce 64 barrels" in {
        assert(new BarrelFactory().numberOfBarrels == 64)
    }

    "The barrelSize is correct" in {
        assert(new BarrelFactory().barrelSize == 67108864)
    }

    "The barrelFactory produces 64 barrels" in {
        val barrels = new BarrelFactory().generateForwardBarrels()
        assert(barrels.length == 64)
    }

    "The getBarrelKeyForWordId works correctly" in {
        val factory = new BarrelFactory()
        val barrels: Array[ForwardBarrel] = factory.generateForwardBarrels()
        val testIds = Array(
            (Int.MinValue, 0),
            (Int.MaxValue, 63),
            (0, 32),
            (-1, 31),
            (1, 32),
            (-1409286145, 10),
            (-1409286144, 11),
        )

        testIds.foreach(x => {
            val barrel: Barrel = barrels(factory.getBarrelKeyForWordId(x._1))
            assert(factory.getBarrelKeyForWordId(x._1) == x._2)
            assert(idFallsInBarrel(x._1, barrel))
        })
    }

    def idFallsInBarrel(id: Int, barrel: Barrel): Boolean = barrel.getStart <= id && barrel.getEnd >= id

}
