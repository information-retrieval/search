package nl.ru.inforetrieval.document

import nl.ru.inforetrieval.hits.CompactHit
import nl.ru.inforetrieval.hits.HitType.{Abstract, Author, Fancy, Plain}
import org.scalatest.freespec.AnyFreeSpec

import scala.collection.mutable.ListBuffer

class DocumentSpec extends AnyFreeSpec {
    "A document" - {
        def id = "docId"

        def title = "Title"

        def authors = "Johnson John"

        def abstr = "Abstract Bod"

        def body = "Body bod bod bodbod"

        s"When initialized with ($id, $title, $authors, $abstr, $body)" - {
            def doc = new Document(id, title, authors, abstr, body)

            "should generate the following hits" in {
                val d = doc

                d.hitLists.get("title") match {
                    case Some(it) => assert(it.equals(ListBuffer(new CompactHit(Fancy, 0))))
                    case _ => fail("could not retrieve 'title'")
                }

                d.hitLists.get("johnson") match {
                    case Some(it) => assert(it.equals(ListBuffer(new CompactHit(Author, 1))))
                    case _ => fail("'could not retrieve 'johnson'")
                }

                d.hitLists.get("abstract") match {
                    case Some(it) => assert(it.equals(ListBuffer(new CompactHit(Abstract, 3))))
                    case _ => fail("'could not retrieve 'abstract'")
                }

                d.hitLists.get("bod") match {
                    case Some(it) => assert(it.equals(ListBuffer(new CompactHit(Abstract, 4), new CompactHit(Plain, 6), new CompactHit(Plain, 7))))
                    case _ => fail("could not retrieve 'body'")
                }
            }
        }
    }
}
