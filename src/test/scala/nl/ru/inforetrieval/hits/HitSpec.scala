package nl.ru.inforetrieval.hits

import nl.ru.inforetrieval.hits.HitType.{Abstract, Author, Fancy, Plain}
import org.scalatest.freespec.AnyFreeSpec

class HitSpec extends AnyFreeSpec {
    "A CompactHit instance" - {
        "stores Plain, Abstract, Author, & Fancy tokens" in {
            val plain = new CompactHit(Plain, 0)
            assert(plain.hitType == Plain)
            val fancy = new CompactHit(Fancy, 0)
            assert(fancy.hitType == Fancy)
            val author = new CompactHit(Author, 0)
            assert(author.hitType == Author)
            val abstr = new CompactHit(Abstract, 0)
            assert(abstr.hitType == Abstract)
        }

        "stores positions up to 30 bits" in {
            val hit = new CompactHit(Plain, 0x3)
            assert(hit.position == 3)
            val overflow = new CompactHit(Plain, 0xffff_ffff)
            assert(overflow.position == 0xffff_ffff)
        }
    }
}
